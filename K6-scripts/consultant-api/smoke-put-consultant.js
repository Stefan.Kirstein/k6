import { sleep } from 'k6';
import http from 'k6/http';

export let options = {
    vus: 1, // max 2 for smoke testing
    duration: '1m',
    thresholds: { http_req_duration: ['p(99)<1500'] }
};

const BASE_URL = 'http://consultant-api-preview.eu-central-1.elasticbeanstalk.com';
export default () => {
    http.get(BASE_URL);

    sleep(3);
}