# Types of load-testing
there are several [types of load-testing](https://k6.io/docs/test-types/introduction) K6 can handle.
## Smoke testing
This is the first step to test the load handling of the application. These tests show if the application can handle a minimum amount of load.
You can execute these tests everytime the application is build to find bottlenecks as early as possible.
## Load testing
After the simple smoke testing passed the load testing is executed. Here, the daily load is tested. The load is set to normal and ramped up to the highest expected load.
Like the smoke test, this test can be executed everytime the application is build.
If this test fails in any cicumstances, it may be because it is used as a stress test.
## Stress testing
When it comes to testing the threshold and robustiness of the application the stress testing comes in place. 
Here the load is increased incrementally to a point where the application is no longer capable in handling the load. This test shows if the application can ramp up its resources to handle the load.
These test should be run at some point when it is not critical, e.g. sunday night.
## Spike testing
This is a variation of the stress testing. The difference is that huge load spikes come at a very short time.

# Running K6 locally
You can execute the scripts locally by installing k6 or running the docker image
## Run by installing
K6 can be installed via Homebrew  
`brew install k6`  
And then simply run the script by \
`k6 run myScript.js`
### Run via Docker
The scripts can also be executed by using the k6 docker image\
`docker run -i loadimpact/k6 run -< myScript.js`
